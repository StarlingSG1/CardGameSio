<?php
namespace App\Core;
/**
 * Class Card : Définition d'une carte à jouer
 * @package App\Core
 */
class Card
{
    /**
     * @var $name string nom de la carte, comme par exemples 'As' '2' 'Reine'
     */
    private $name;
    /**
     * @var $color string couleur de la carte, par exemples 'Pique', 'Carreau'
     */
    private $color;
    /**
     * Card constructor.
     * @param string $name
     * @param string $color
     */
    public function __construct(string $name, string $color)
    {
        $this->name = $name;
        $this->color = $color;
    }
    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }
    public function getColor(): string
    {
        return $this->color;
    }
    /**
     * @param string $name
     */
    public function setName(string $name): void
    {
        $this->name = $name;
    }
    public function setColor(string $color): void
    {
        $this->color = $color;
    }
    /** définir une relation d'ordre entre instance de Card.
     *  Remarque : cette méthode n'est pas de portée d'instance
     *
     * @see https://www.php.net/manual/fr/function.usort.php
     *
     * @param $o1 Card
     * @param $o2 Card
     * @return int
     * <ul>
     *  <li> zéro si $o1 et $o2 sont considérés comme égaux </li>
     *  <li> -1 si $o1 est considéré inférieur à $o2</li>
     * <li> +1 si $o1 est considéré supérieur à $o2</li>
     * </ul>
     *
     */

    const colorArray =
        ["Carreau" => 40,
        "Pique" => 30,
        "Trèfle" => 20,
        "Coeur" => 10];

    const nameArray =
        ["2" => 2,
        "3" => 3,
        "4" => 4,
        "5" => 5,
        "6" => 6,
        "7" => 7,
        "8" => 8,
        "9" => 9,
        "10" => 10,
        "Valet" => 11,
        "Dame" => 12,
        "Roi" => 13,
        "As" => 14];

    public static function cmp(Card $o1, Card $o2) : int
    {
        $o1Name = $o1->name; //1
        $o2Name = $o2->name;
        $o1Color = $o1->color;
        $o2Color = $o2->color;

        // Si nom est bon, on verifie la couleur, si le nom est pas bon on verifie le nom
        if (self::nameArray[$o1Name] == self::nameArray[$o2Name] ){ //2
            if(self::colorArray[$o1Color] == self::colorArray[$o2Color] ) { //3
                return 0; //4
            }
            return(self::colorArray[$o1Color] > self::colorArray[$o2Color]) ? +1 : -1; //5
        }
        return (self::nameArray[$o1Name] > self::nameArray[$o2Name]) ? +1 : -1; //6
    }


    public function toString()
    {
        return $this->getName(). " de ". $this->getColor();
    }



}
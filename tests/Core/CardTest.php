<?php

namespace App\Tests\Core;

use PHPUnit\Framework\TestCase;
use App\Core\Card;

class CardTest extends TestCase
{

    public function testName()
    {
        $card = new Card('As', 'Trèfle');
        $this->assertEquals('As', $card->getName());
        $card = new Card('2', 'Trèfle');
        $this->assertEquals('2', $card->getName());

    }



    public function testColor()
    {
        $card = new Card('2', 'trèfle');
        $this->assertEquals('trèfle', $card->getColor());
    }

    public function testCmp()
    {
        //                  Carte 0     Couleur 0
        $card1 = new Card('3', 'Pique');
        $card2 = new Card('3', 'Pique');
        $this->assertEquals(0, Card::cmp($card1, $card2));
        //                  Carte 0     Couleur +1
        $card1 = new Card('3', 'Carreau');
        $card2 = new Card('3', 'Pique');
        $this->assertEquals(+1, Card::cmp($card1, $card2));
        //                  Carte 0     Couleur -1
        $card1 = new Card('3', 'Coeur');
        $card2 = new Card('3', 'Pique');
        $this->assertEquals(-1, Card::cmp($card1, $card2));
        //                  Carte +1     Couleur 0
        $card1 = new Card('4', 'Pique');
        $card2 = new Card('3', 'Pique');
        $this->assertEquals(+1, Card::cmp($card1, $card2));
        //                  Carte +1     Couleur 0
        $card1 = new Card('9', 'Pique');
        $card2 = new Card('8', 'Pique');
        $this->assertEquals(+1, Card::cmp($card1, $card2));
        //                  Carte -1     Couleur -1
        $card1 = new Card('7', 'Coeur');
        $card2 = new Card('8', 'Pique');
        $this->assertEquals(-1, Card::cmp($card1, $card2));
        //                  Carte +1     Couleur -1
        $card1 = new Card('9', 'Pique');
        $card2 = new Card('8', 'Carreau');
        $this->assertEquals(+1, Card::cmp($card1, $card2));
        //                  Carte -1     Couleur +1
        $card1 = new Card('7', 'Carreau');
        $card2 = new Card('8', 'Pique');
        $this->assertEquals(-1, Card::cmp($card1, $card2));
        //                  Carte +1     Couleur +1
        $card1 = new Card('9', 'Carreau');
        $card2 = new Card('8', 'Pique');
        $this->assertEquals(+1, Card::cmp($card1, $card2));
    }



    public function testToString()
    {
        $card = new Card('As', 'Pique');
        $this->assertEquals('As de Pique', $card->toString());
        $card = new Card('3', 'Coeur');
        $this->assertEquals('3 de Coeur', $card->toString());
    }

}
// TestCmpPourChaqueCas ouioui nonnon ouinon nonoui
// Mettre algo du dessus dans le rapport (chaque cas) et dessiner le rapport de force des cartes
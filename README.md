# Guess What

Prise en main de la POO avec PHP

Niveau : Deuxième année de BTS SIO SLAM

Prérequis : bases de la programmation, PHP 7 ou supérieur installé sur votre machine de dev.  

## Thème 

Développer une logique de jeu puis l'adapter progressivement (en _refactoring_) à un contexte d'application web (avec symfony)   
 
Les étapes d'un scénario typique d'usage sont 

1. (optionnel pour le joueur) paramétrage du jeu (par exemple choix du jeu de cartes, aide à la recherche, ...)
2. Lancement d'une partie (le jeu tire une carte "au hasard"), que le joueur doit deviner en un temps "optimal"
3. Le joueur propose une carte  
4. Si c'est la bonne carte alors la partie se termine et le jeu affiche des éléments d'analyse (nombre de fois
 où le joueur a soumis une carte, sa qualité stratégique, ...)
* Si ce n'est pas la bonne carte, alors si l'aide est activée, le joeur est informé si la carte qu'il a soumise est 
plus petite ou plus grande que celle à deviner. Retour en 3.

## Objectif

* Mise au point de la logique applicative avec PHPUnit
* Notion de structure de données, recherche d'un élement dans une liste 
* Analyse des actions du joueur (fonction du nombre de cartes, aides à la décision)  

## Premiers éléments d'analyse 



* La classe `Guess` est responsable de la logique du jeu. C'est dans cette classe que la structure du jeu de carte est créée.
* La classe `Card` définit la structure d'une carte à jouer. 

Une instance de `Guess` est relièe, à un instant _t_, à un ensemble de cartes
 (`cards`) et à une instance de `Card` (`selectedCard` est la carte que le joueur doit deviner)   

Ce projet est aussi composé de deux autres classes : CardTest qui test la logique des cartes, et GuessTest qui test la logique du jeu.

## Répartition des tâches
Pour ce projet, notre manière de travailler était la suivante : nous réfléchissions à la tache à effectuer, nous établissions la logique à suivre, nous faisons des essaies chacun de notre coté, et au bout d'un certain temps, mettons en commun nos réfléxions, essais concluants ou non, et avançons ensemble jusqu'à trouver la solution à notre problème.



## Analyse du code
Une fois les prérequis installés, la premiere mission fut de construire les TODOS présents dans les fichiers Card.php (src/Core/Card.php) et CardTest.php (tests/Core/CardTest.php)
Dans le fichier Card.php se trouve la classe Card qui créer un objet carte :

'''php 

